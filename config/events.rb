WebsocketRails::EventMap.describe do
  subscribe :user_loggedin,         'event#user_loggedin'
  subscribe :new_post,              'event#new_post'
  subscribe :new_comment,           'event#new_comment'
  subscribe :new_like,              'event#new_like'
  subscribe :new_unlike,            'event#new_unlike'
  subscribe :channel_subscribe,     'event#channel_subscribe'
  subscribe :channel_unsubscribe,   'event#channel_unsubscribe'
  subscribe :client_connected,      'event#client_connected'
  subscribe :client_disconnected,   'event#client_disconnected'
  subscribe :create_channel,        'event#create_channel'

  # You can use this file to map incoming events to controller actions.
  # One event can be mapped to any number of controller actions. The
  # actions will be executed in the order they were subscribed.
  #
  # Uncomment and edit the next line to handle the client connected event:
  #   subscribe :client_connected, :to => Controller, :with_method => :method_name
  #
  # Here is an example of mapping namespaced events:
  #   namespace :product do
  #     subscribe :new, :to => ProductController, :with_method => :new_product
  #   end
  # The above will handle an event triggered on the client like `product.new`.
end
