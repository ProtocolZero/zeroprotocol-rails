require File.expand_path('../boot', __FILE__)

require 'rails/all'
require "sprockets/railtie"
# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Zeroprotocol
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    config.time_zone = 'Mumbai'
    config.active_record.default_timezone = 'local'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de
    config.generators do |g|
      g.orm             :active_record
      g.template_engine :erb
      g.test_framework  false
      g.stylesheets     false
      g.helper          false
      g.javascripts     false
    end
    config.encoding = "utf-8"

    if defined? ::HamlCoffeeAssets
      config.hamlcoffee.templates_path = "app/assets/javascripts/client/templates"
      config.hamlcoffee.escapeAttributes = false
      config.hamlcoffee.uglify = true
      config.hamlcoffee.name_filter = lambda do |n|
        return n.gsub(/client\/templates\//, '').gsub(/(\/[_])(?<file_name>[^\/]+)$/, '/\k<file_name>')
      end
    end

    Dir["#{Rails.root}/config/clientroutes.rb"].each do |route_file|
      config.paths['config/routes.rb'] << route_file
    end

  end
end
