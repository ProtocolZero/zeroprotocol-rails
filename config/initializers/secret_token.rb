# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Zeroprotocol::Application.config.secret_key_base = 'd2b534be0c2eefe2c74006e3a05826767b6162a24f01348177fd321d116c1d333a5021780ab9fa029a16f4ed3487549ef478886a9d6287d8d6d82463a7165854'
