# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

organisations = [
  { :name => "Housing", :email_identifier => "housing.com" }
]
organisations.each do |org|
  org = Organisation.where(org).first_or_create
end
