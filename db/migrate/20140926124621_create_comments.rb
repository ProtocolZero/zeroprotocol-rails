class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.integer :commenter_id
      t.references :post, index: true
      t.text :content
      t.boolean :private

      t.timestamps
    end
  end
end
