class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.string :email
      t.string :contact_no
      t.string :password
      t.references :organisation, index: true
      t.datetime :last_login

      t.timestamps
    end
  end
end
