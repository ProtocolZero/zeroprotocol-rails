class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.integer :poster_id
      t.references :channel, index: true
      t.text :content
      t.boolean :private

      t.timestamps
    end
  end
end
