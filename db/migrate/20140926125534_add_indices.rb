class AddIndices < ActiveRecord::Migration
  def change
  	add_index :channels, :name, unique: true
  	add_index :comments, :commenter_id
  	add_index :comments, :private
  	add_index :organisations, :name, unique: true
  	add_index :organisations, :email_identifier, unique: true
  	add_index :posts, :poster_id
  	add_index :posts, :private
  	add_index :users, :email, unique: true
  	add_index :users, :contact_no, unique: true
  end
end
