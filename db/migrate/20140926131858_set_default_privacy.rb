class SetDefaultPrivacy < ActiveRecord::Migration
  def change
  	change_column :posts, :private, :boolean, :default => false
  	change_column :comments, :private, :boolean, :default => false
  end
end
