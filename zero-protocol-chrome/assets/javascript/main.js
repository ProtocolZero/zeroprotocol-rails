var housing = {

  hackthon: function() {
    this.bind_events();
    housing.checkLogin();
  },
  bind_events: function(){
    var input = document.getElementById('login-btn');
    var fake_email = 'kailash.bagaria@housing.com';
    var fake_pwd = "b";

    document.body.onkeydown =  function(e){
      if (e.keyCode == 13){
        var email = document.getElementById('email').value;
        var pwd = document.getElementById('password').value;
        if (fake_email == email && fake_pwd == pwd){
          var user = {
            "email" : email
          }
          window.localStorage.setItem("logged_in_token", JSON.stringify(user));
          housing.checkLogin();
        }
      }
    };
    input.addEventListener('click', function() {
      var email = document.getElementById('email').value;
      var pwd = document.getElementById('password').value;
      if (fake_email == email && fake_pwd == pwd){
        housing.checkLogin();
      }
    });
  },

  checkLogin: function(){
    var logged_in_token = window.localStorage.getItem("logged_in_token");
    if (logged_in_token != "false" && logged_in_token.length){
      document.body.className = "logged-in";
      this.WebSocket  = new SocketLayer();
      this.options = {
        "host" : "kailash.housing.com",
        "port" : 3632
      };
      var user = JSON.parse(logged_in_token);
      var scriptTag = document.createElement("script");
      scriptTag.src = "./assets/javascript/list.js";
      document.getElementsByTagName("head")[0].appendChild(scriptTag);
      this.WebSocket.connect(this.options);
      this.WebSocket.sendLogin(user);
    } else {
      document.body.className = "";
    }
  },
};

document.addEventListener('DOMContentLoaded', function () {
  document.getElementById('email').value = 'kailash.bagaria@housing.com';
  housing.hackthon();
});
