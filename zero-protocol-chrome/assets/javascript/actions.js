/*

	Actions

*/

var Actions = function () {
	if (this instanceof Actions)
		return this;
	else
		return new Actions();
}

Actions.prototype.append_feed = function(data) {
	window.user = data.user;
	window.subscribed_channelList = data.channels;
	var login_screen = document.getElementById('login_zero_screen');
	var landing_screen = document.getElementById('login_first_screen');  
	if (login_screen)
		login_screen.style.display = "none";
	landing_screen.style.display = 'block';
	this.goto_landing_page();
	var feeds = data.feeds;
	var feed_container = document.getElementById("feeds_list");
	var self = this;
  	for ( var i=0; i<feeds.length; i++ ){
  		var feed = window.JST['website/list'](feeds[i]);
  		var dom = document.createElement("li");
  		dom.className = 'feed';
  		dom.innerHTML = feed;
  		dom.p_id = feeds[i].id;
  		dom.setAttribute("post_id", feeds[i].id);
  		dom.onclick = (function(dom){
  			var d = dom;
  			return function(e){
  				self.bind_feed(d, e);
  			}
  		})(dom);
  		feed_container.appendChild(dom);
  	}
  	this.append_channels(data);
}

Actions.prototype.goto_landing_page = function(){
	var title = document.getElementById('main-heading');
	var user_feed = document.getElementById('user-feed');
	var menu_list = document.getElementById('menu-details');	
	user_feed.style.display = 'block';
	menu_list.style.display = 'none';
	title.addEventListener('click', function(){
		user_feed.style.display = 'block';
		menu_list.style.display = 'none';		
	});
}

Actions.prototype.append_channels = function(data){
	var subscribed = data.channels;
	var unsubscribed = data.unsubscribed_channels;
	var channels_container = document.getElementById("channels_list");
  	for ( var i=0; i<subscribed.length; i++ ){
  		var channel = window.JST['website/channel'](subscribed[i]);
  		var dom = document.createElement("li");
  		dom.className = 'channel subscribed';
  		dom.dataset.id = subscribed[i].id;
  		dom.dataset.count_id = subscribed[i].count;  		
  		dom.innerHTML = channel;
  		dom.p_id = subscribed[i].id;
  		dom.setAttribute("post_id", subscribed[i].id);
  		dom.onclick = (function(dom){
  			var d = dom;
  			return function(e){
  				window.open("http://rahuly.housing.com:57584/user-feed#"+d.getAttribute("post_id"));
  			}
  		})(dom);    		
  		channels_container.appendChild(dom);
  	}  	
  	for ( var i=0; i<unsubscribed.length; i++ ){
  		var channel = window.JST['website/channel'](unsubscribed[i]);
  		var dom = document.createElement("li");
  		dom.className = 'channel';
  		dom.dataset.id = unsubscribed[i].id;
  		dom.dataset.count_id = unsubscribed[i].count;  		
  		dom.innerHTML = channel;
  		dom.p_id = unsubscribed[i].id;
  		dom.setAttribute("post_id", unsubscribed[i].id);
  		dom.onclick = (function(dom){
  			var d = dom;
  			return function(e){
  				window.open("http://rahuly.housing.com:57584/user-feed#"+d.getAttribute("post_id"));
  			}
  		})(dom);    		
  		channels_container.appendChild(dom);
  	}  	  	
  	this.bind_further();
}

Actions.prototype.bind_further = function(){
	var self = this;
	var feeds = document.getElementById('feeds');
	var channels = document.getElementById('channels');
	var feeds_list = document.getElementById('feeds_list');
	var channels_list = document.getElementById('channels_list');
	var menu = document.getElementById('menu');
	var user_feed = document.getElementById('user-feed');
	var menu_list = document.getElementById('menu-details');
	feeds.addEventListener('click', function() {
	  feeds.className = "feeds selected"
	  channels.className = "channels"
	  feeds_list.style.display = 'block';
	  channels_list.style.display = 'none';
	});
	channels.addEventListener('click', function() {
	  feeds.className = "feeds"
	  channels.className = "channels selected"      
	  feeds_list.style.display = 'none';
	  channels_list.style.display = 'block';
	});
	menu.addEventListener('click', function() {
	  user_feed.style.display = 'none';
	  menu_list.style.display = 'block';
	  self.bind_menu();
	});	
	self.bind_channel();
}


Actions.prototype.logOut = function(){
	window.localStorage.setItem("logged_in_token", "false");
	var login_screen = document.getElementById('login_zero_screen');
	var landing_screen = document.getElementById('login_first_screen');  
	if (login_screen)
		login_screen.style.display = "block";
	landing_screen.style.display = 'none';
}

Actions.prototype.bind_menu = function(){
	self = this;
	var subscribed_channels = document.getElementById('subscribed-channels');
	var logout = document.getElementById('logout-btn');
	subscribed_channels.addEventListener('click', function(){
		var subscribed_list = document.getElementById('subscribed-list');
		var menu_list = document.getElementById('menu-list');
		subscribed_list.style.display = 'block';
		menu_list.style.display = 'none';
		self.append_subscribed();
	});
	logout.addEventListener('click', function(){
		self.logOut();
	});
}

Actions.prototype.append_subscribed = function(){
	var subscribed = window.subscribed_channelList;
	var channels_container = document.getElementById("subscribed-list");

  	for ( var i=0; i<subscribed.length; i++ ){
  		var channel = window.JST['website/channel'](subscribed[i]);
  		var dom = document.createElement("li");
  		dom.className = 'channel subscribed';
  		dom.dataset.id = subscribed[i].id;
  		dom.dataset.count_id = subscribed[i].count;
  		dom.innerHTML = channel;
  		dom.p_id = subscribed[i].id;
  		dom.setAttribute("post_id", subscribed[i].id);
  		dom.onclick = (function(dom){
  			var d = dom;
  			return function(e){
  				window.open("http://rahuly.housing.com:57584/user-feed#"+d.getAttribute("post_id"));
  			}
  		})(dom);  		
  		channels_container.appendChild(dom);
  	}  	
  	this.bind_subscribed_channel();
}

Actions.prototype.bind_subscribed_channel = function(){
	var self = this;
	self.bind_channel();
}

Actions.prototype.bind_feed = function(d, e){
	window.open("http://rahuly.housing.com:57584/user-feed#"+d.getAttribute("post_id"));
}

Actions.prototype.bind_channel = function(){
	//write events for click on feed to take to the feed
	self = this;
	var subscribe = document.getElementsByClassName('subscribe');
	var unsubscribe = document.getElementsByClassName('unsubscribe');
	var logged_in_token = window.localStorage.getItem("logged_in_token");
	this.WebSocket  = new SocketLayer();
	this.options = {
		"host" : "rahuly.housing.com",
		"port" : 57584
	};
	var user = JSON.parse(logged_in_token);
	var scriptTag = document.createElement("script");
	scriptTag.src = "./assets/javascript/list.js";
	document.getElementsByTagName("head")[0].appendChild(scriptTag);
	this.WebSocket.connect(this.options);	
	for(var i=0; i<subscribe.length; i++){
		subscribe[i].addEventListener('click', function(e){
			obj = {};
			obj.channel_id = parseInt(e.currentTarget.parentElement.parentElement.dataset.id);
			obj.count = parseInt(e.currentTarget.parentElement.parentElement.dataset.count_id);
			obj.user_id = window.user.id;
			self.WebSocket.subscribe(obj);
			self.on_subscribe(e.currentTarget.parentElement.parentElement);
			e.stopPropagation();
			e.preventDefault();			
		});
	}
	for(var i=0; i<unsubscribe.length; i++){
		unsubscribe[i].addEventListener('click', function(e){
			obj = {};
			obj.channel_id = parseInt(e.currentTarget.parentElement.parentElement.dataset.id);
			obj.count = parseInt(e.currentTarget.parentElement.parentElement.dataset.count_id);
			obj.user_id = window.user.id;
			self.WebSocket.unsubscribe(obj);
			self.on_unsubscribe(e.currentTarget.parentElement.parentElement);
			e.stopPropagation();
			e.preventDefault();
		});	
	}
}

Actions.prototype.on_subscribe = function(elem){
	elem.className = "channel subscribed";
}

Actions.prototype.on_unsubscribe = function(elem){
	elem.className = "channel"
}