(function() {
  if (window.JST == null) {
    window.JST = {};
  }

  window.JST['website/list'] = function(context) {
    return (function() {
      var $c, $e, $o;
      $e = function(text, escape) {
        return ("" + text).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/'/g, '&#39;').replace(/\//g, '&#47;').replace(/"/g, '&quot;');
      };
      $c = function(text) {
        switch (text) {
          case null:
          case void 0:
            return '';
          case true:
          case false:
            return '' + text;
          default:
            return text;
        }
      };
      $o = [];
      $o.push("<div class='feed-element'>\n  <div class='feed_person'>");
      if (this.poster_name && this.poster_name.length !== 0) {
        $o.push("    " + $e($c(this.poster_name)));
      } else {
        $o.push("Anonymous");
      }
      $o.push("  </div>\n  <img class='feed_img' src='./assets/images/photo.png'>\n  <div class='feed_title'>");
      $o.push("    " + $e($c(this.content)));
      $o.push("  </div>\n</div>\n<div class='action-elements'>\n  <i class='fa-heart-o feed_like icon' id='like'>");
      $o.push("    " + $e($c(this.likes_count)));
      $o.push("  </i>\n  <i class='fa-comment-o feed_comment icon' id='comment'>");
      $o.push("    " + $e($c(this.comments.length)));
      $o.push("  </i>\n</div>");
      return $o.join("\n").replace(/\s([\w-]+)='true'/mg, ' $1').replace(/\s([\w-]+)='false'/mg, '').replace(/\s(?:id|class)=(['"])(\1)/mg, "");
    }).call(context);
  };

}).call(this);

//this is channels HAML
(function() {
  if (window.JST == null) {
    window.JST = {};
  }

  window.JST['website/channel'] = function(context) {
    return (function() {
      var $c, $e, $o;
      $e = function(text, escape) {
        return ("" + text).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/'/g, '&#39;').replace(/\//g, '&#47;').replace(/"/g, '&quot;');
      };
      $c = function(text) {
        switch (text) {
          case null:
          case void 0:
            return '';
          case true:
          case false:
            return '' + text;
          default:
            return text;
        }
      };
      $o = [];
      $o.push("<div class='channel-icon'>\n  <i class='fa-paper-plane icon'></i>\n</div>\n<div class='channel-element'>\n  <div class='channel_title'>");
      $o.push("    " + $e($c(this.name)));
      $o.push("  </div>\n</div>\n<div class='action-elements'>\n  <i class='fa-user icon num_of_users'>");
      $o.push("    " + $e($c(this.count)));
      $o.push("  </i>\n  <i class='fa-bell-o icon subscribe' id='subscribe'>\n    Subscribe\n  </i>\n  <i class='fa-bell-slash-o icon unsubscribe' id='unsubscribe'>\n    Unsubscribe\n  </i>\n</div>");
      return $o.join("\n").replace(/\s([\w-]+)='true'/mg, ' $1').replace(/\s([\w-]+)='false'/mg, '').replace(/\s(?:id|class)=(['"])(\1)/mg, "");
    }).call(context);
  };

}).call(this);

(function() {
  if (window.JST == null) {
    window.JST = {};
  }

  window.JST['website/feed'] = function(context) {
    return (function() {
      var $c, $e, $o, comment, _i, _len, _ref;
      $e = function(text, escape) {
        return ("" + text).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/'/g, '&#39;').replace(/\//g, '&#47;').replace(/"/g, '&quot;');
      };
      $c = function(text) {
        switch (text) {
          case null:
          case void 0:
            return '';
          case true:
          case false:
            return '' + text;
          default:
            return text;
        }
      };
      
      $o = [];
      $o.push("<div class='feed-element'>\n  <div class='feed_person'>");
      $o.push("    " + $e($c(this.data.user.name)));
      $o.push("  </div>\n  <div class='feed_img' src='./assets/images/photo.png'></div>\n  <div class='feed_title'>");
      $o.push("    " + $e($c(this.feed.content)));
      $o.push("  </div>\n</div>\n<div class='action-elements'>\n  <i class='fa-heart-o feed_like icon' id='like'>");
      $o.push("    " + $e($c(this.feed.likes_count)));
      $o.push("  </i>\n  <i class='fa-comment-o feed_comment icon' id='comment'>");
      $o.push("    " + $e($c(this.feed.comments.length)));
      $o.push("  </i>\n</div>\n<div class='comments-holder'>");
      _ref = this.comments;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        comment = _ref[_i];
        $o.push("  <div class='comment-wrapper'>\n    <div class='user-name'></div>");
        $o.push("    " + $e($c(comment.name)));
        $o.push("    <div class='text'>");
        $o.push("      " + $e($c(comment.text)));
        $o.push("    </div>\n  </div>");
      }
      $o.push("</div>\n<div class='comment-holder'>\n  <textarea id='comment-box'></textarea>\n  <i class='fa-lock icon-lock'></i>\n</div>");
      return $o.join("\n").replace(/\s([\w-]+)='true'/mg, ' $1').replace(/\s([\w-]+)='false'/mg, '').replace(/\s(?:id|class)=(['"])(\1)/mg, "");
    }).call(context);
  };

}).call(this);

