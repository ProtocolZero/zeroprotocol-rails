/*
	Socket layer
*/

var SocketLayer = function(){
	if (this instanceof SocketLayer)
		return this;
	else
		return new SocketLayer();
}
SocketLayer.prototype.connect = function(options){
	this.socket_url = url = options.host + ':' + options.port + '/websocket';
    this.use_websockets = true;
    this.Dispatcher = new WebSocketRails(this.socket_url, this.use_websockets);
    return this.Dispatcher;
}

SocketLayer.prototype.sendLogin = function(userDetails){
	this.Dispatcher.trigger("user_loggedin", userDetails, this.loginCallback, this.loginfailCallback);
}

SocketLayer.prototype.loginCallback = function(data){
	this.Actions = new Actions();
	this.Actions.append_feed(data);
}

SocketLayer.prototype.loginfailCallback = function(data){
	console.log('Error! User Not Found');
}

SocketLayer.prototype.subscribe = function(obj){
	this.Dispatcher.trigger('channel_subscribe', obj, this.on_channel_subscribe_success, this.on_channel_subscribe_failure);
}

SocketLayer.prototype.on_channel_subscribe_success = function(data){
	console.log('success');
}

SocketLayer.prototype.on_channel_subscribe_failure = function(data){
	console.log('Error! User Not Found');
}

SocketLayer.prototype.unsubscribe = function(obj){
	this.Dispatcher.trigger('channel_unsubscribe', obj, this.on_channel_unsubscribe_success, this.on_channel_unsubscribe_failure);
}

SocketLayer.prototype.on_channel_unsubscribe_success = function(data){
	console.log('unsuccess');
}

SocketLayer.prototype.on_channel_unsubscribe_failure = function(data){
	console.log('unError! User Not Found');
}