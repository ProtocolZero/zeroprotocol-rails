define ['client/global_bus'], (ZP)->

	class EventBusView extends Backbone.View

		initialize: ()=>
			console.log('in channels view')

		click_login_submit: ()=>
			ZP.is_user_loggedIn = true
			url = '/channels'
			ZP.router.goto_url(url, )

		destroy: ()=>
			@stopListening()
			console.log('destroyed')

	return EventBusView