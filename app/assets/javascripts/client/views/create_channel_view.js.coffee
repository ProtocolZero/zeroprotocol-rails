define ['client/global_bus',
		'client/templates/client/_channel',
		'client/templates/client/_unsubscribe_channel'
		], (ZP)->

	class CreateChannelView extends Backbone.View

		el: '.channels-list'

		templates:
			subscribed: window.JST['client/channel']
			unsubscribed: window.JST['client/unsubscribe_channel']

		initialize: ()=>
			@$el.find('.create-channel').on('click', @create_new_channel)
			@$el.find('.submit-new-channel').on('click', @dispatch_channel_creation)
			@$el.find('.subscribe-action').on('click', @subscribe_user)
			@$el.find('.unsubscribe-action').on('click', @unsubscribe_user)
			ZP.Dispatcher.bind('create_channel', @on_new_channel)
			ZP.Dispatcher.bind('channel_subscribe', @on_new_channel_subscribe)
			ZP.Dispatcher.bind('channel_unsubscribe', @on_new_channel_unsubscribe)
			@listenTo ZP.event, 'update:click_listener', @update_listeners

		create_new_channel: (e)=>
			if not @$el.find('.create-channel').hasClass('fade-color')
				@$el.find('.create-channel').addClass('fade-color')
				@$el.find('.create-new-channel').show()
			else if @$el.find('.create-channel').hasClass('fade-color')
				@$el.find('.create-channel').removeClass('fade-color')
				@$el.find('.create-new-channel').hide()

		update_listeners: ()=>
			$('.subscribe-action').off('click', @subscribe_user)
			$('.unsubscribe-action').off('click', @unsubscribe_user)
			$('.subscribe-action').on('click', @subscribe_user)
			$('.unsubscribe-action').on('click', @unsubscribe_user)

		on_channel_creation_success: (data)=>
			@$el.find('.create-new-channel').hide()
			console.log('on channel creation success')

		on_channel_creation_failure: (err)=>
			console.log('on channel creation failure')

		dispatch_channel_creation: (e)=>
			if @$el.find('.new-channel-input').val() != "" and @$el.find('.org-select').val() != ""
				channel_name = $('.new-channel-input').val()
				channel_name.trim(' ')
				if channel_name[0] == "#"
					channel_name.replace('#', '')
				org_id = parseInt(@$el.find('.org-select').val())
				obj =
					name: channel_name
					organisation_id: org_id
					user_id: ZP.User.get('id')
				@$el.find('.create-channel').removeClass('fade-color')
				@$el.find('.new-channel-input').val('')
				ZP.Dispatcher.trigger('create_channel', obj, @on_channel_creation_success, @on_channel_creation_failure)
				console.log('hi')

		update_channels_collection: (data, add=true)=>
			if data.count < 0
				data.count = 0
			ZP.Channels.each (channel)=>
				ZP.ChannelListeners[channel.id].destroy()
			if add
				ZP.Channels.add(data, {merge: true})
			else if ZP.Channels.get(data.id)
				ZP.Channels.remove([data])
			ZP.Channels.each (channel)=>
				ZP.ChannelListeners[channel.id] = ZP.Dispatcher.subscribe(channel.id)
			ZP.event.trigger 'update:channels'

		on_new_channel: (data)=>
			if data.user_id != ZP.User.get('id')
				$('.unsubscribed-channel-list').prepend(@templates['unsubscribed'](channel: data))
				@update_channels_collection(data, false)
				if data.count < 0
					data.count = 0
				ZP.Unsubscribed.add(data, {merge:true})
			else if data.user_id == ZP.User.get('id')
				@update_channels_collection(data, true)
				$('.subscribed-channel-list').prepend(@templates['subscribed'](channel: data))
			ZP.event.trigger 'update:click_listener'
			@update_listeners()

		on_channel_subscribe_success: (data)=>
			console.log('on channel subscribe success', data)

		on_channel_suubscribe_failure: (err)=>
			console.log('on channel subscribe failure', err)

		subscribe_user: (e)=>
			if e and $(e.currentTarget) and $(e.currentTarget).data('channelid')
				channel_id  = $(e.currentTarget).data('channelid')
				user_id = ZP.User.get('id')
				channel_count = ZP.Unsubscribed.get(channel_id).get('count')
				obj =
					channel_id: channel_id
					user_id: user_id
					count: channel_count
				console.log(obj)
				ZP.Dispatcher.trigger('channel_subscribe', obj, @on_channel_subscribe_success, @on_channel_suubscribe_failure)

		on_new_channel_subscribe: (data)=>
			if data.user_id == ZP.User.get('id')
				@$el.find('.channel-container[data-channelid='+data.id+']').remove()
				@$el.find('.subscribed-channel-list').prepend(@templates['subscribed'](channel: data))
				@update_channels_collection(data, true)
				ZP.event.trigger 'update:click_listener'
				@update_listeners()
			else if data.user_id != ZP.User.get('id')
				@$el.find('.channel-container[data-channelid='+data.id+'] .channel-keep-count').text(data.count)

		on_channel_unsubscribe_success: (data)=>
			console.log('on channel subscribe success', data)

		on_channel_unsuubscribe_failure: (err)=>
			console.log('on channel subscribe failure', err)

		unsubscribe_user: (e)=>
			if e and $(e.currentTarget) and $(e.currentTarget).data('channelid')
				channel_id  = $(e.currentTarget).data('channelid')
				user_id = ZP.User.get('id')
				channel_count = ZP.Channels.get(channel_id).get('count')
				obj =
					channel_id: channel_id
					user_id: user_id
					count: channel_count
				ZP.Dispatcher.trigger('channel_unsubscribe', obj, @on_channel_subscribe_success, @on_channel_suubscribe_failure)

		on_new_channel_unsubscribe: (data)=>
			if data.user_id == ZP.User.get('id')
				@$el.find('.channel-container[data-channelid='+data.id+']').remove()
				@$el.find('.unsubscribed-channel-list').prepend(@templates['unsubscribed'](channel: data))
				@update_channels_collection(data, false)
				if data.count < 0
					data.count = 0
				ZP.Unsubscribed.add(data, {merge: true})
				feeds_to_remove = ZP.Feeds.where({channel_id: data.id})
				feeds_to_remove.forEach (feed)=>
					$('.feed-container[data-feedid='+feed.get("id")+']').remove()
			else if data.user_id != ZP.User.get('id')
				@$el.find('.channel-container[data-channelid='+data.id+'] .channel-keep-count').text(data.count)
			ZP.event.trigger 'update:click_listener'

		destroy: ()=>
			@$el.find('.create-channel').off('click', @create_new_channel)
			@$el.find('.submit-new-channel').off('click', @dispatch_channel_creation)
			@$el.removeData().unbind()
			@stopListening()

	return CreateChannelView