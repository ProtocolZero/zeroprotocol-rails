define ['client/global_bus'], ()->

	class TextAreaTypeView extends Backbone.View

		initialize: ()=>
			@$el = @options.el
			@$el.on('keydown', @on_change)
			@initial_height = @$el.height()

		on_change: (e) =>
			if $(e.currentTarget).val() == ""
				@$el.attr('rows',1)

			if !@scroll_height
				@scroll_height = e.target.scrollHeight
			if @scroll_height and (@scroll_height != e.target.scrollHeight)
				@scroll_height = e.target.scrollHeight
				rows = parseInt(@$el.attr('rows'))
				rows +=1
				console.log(rows)
				@$el.attr('rows',rows)

		destroy:()=>
			@$el.off('keydown', @on_change)

	return TextAreaTypeView