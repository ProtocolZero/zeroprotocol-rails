define ['client/global_bus',
		'client/templates/client/_login'
		], (ZP)->

	class LoginView extends Backbone.View

		el: '.login-container'

		templates:
			main: window.JST['client/login']

		initialize: ()=>
			if !$('#container').find('.login-container').length
				$('#container').append(@templates['main']())
			@$el.find('#loginSubmit').bind('click', @click_login_submit)
			@$el.find('#loginSubmit').bind('submit', @click_login_submit)
			if ZP.Util.is_localstorage_accessible() and ZP.Util.read_localStorage('user-loggedin')
				email =  ZP.Util.read_localStorage('email')
				ZP.Util.make_call(email)

		click_login_submit: (e)=>
			@supress(e)
			url = '/channels'
			email = $('#loginEmail').val()
			email = '' if !email
			ZP.Util.make_call(email)

		supress: (e)=>
			if e
				e.preventDefault()
				e.stopPropagation()

		destroy: ()=>
			@$el.removeData().unbind()
			$('.login-container').remove()
			@$el.find('#loginSubmit').unbind('click', @click_login_submit)
			@stopListening()

	return LoginView