define ['client/global_bus',
		'client/views/search_view',
		'client/views/textaera_type'
		'client/templates/client/_feed'
		], (ZP, SearchView, TextAreaTypeView)->

	class CreatePostView extends Backbone.View

		el: '.create-post-elem'
		templates:
			feed: window.JST['client/feed']

		initialize: ()=>
			elem = $('.post-create-input-search')
			@listenTo ZP.event, 'update:click_listener', @update_listener
			@listenTo ZP.event, 'update:channels', @update_channels
			@search_view = new SearchView
				el : elem
				options_array: ZP.Channels.toJSON()
				namespace: 'create_post'

			@textaera_type_view = new TextAreaTypeView
				el : $('.create-post-elem .create-post-input')

			@listenTo ZP.event, 'channel:selected:create_post', @select_channel
			$('.create-post-elem .send-post-to-create-container').on('click', @create_post)
			$('.create-post-elem .change-privacy-flag').on('click', @change_privacy_flag)
			$('.post-create-input-search').on('change', @on_channel_input_change)
			$('.create-post-elem .user-signout').on('click', @logout)
			@update_channels()

		update_listener: ()=>
			$('.create-post-elem .send-post-to-create-container').off('click', @create_post)
			$('.create-post-elem .change-privacy-flag').off('click', @change_privacy_flag)
			$('.post-create-input-search').off('change', @on_channel_input_change)
			$('.create-post-elem .send-post-to-create-container').on('click', @create_post)
			$('.create-post-elem .change-privacy-flag').on('click', @change_privacy_flag)
			$('.post-create-input-search').on('change', @on_channel_input_change)

		logout: ()=>
			if ZP.Util.is_localstorage_accessible()
				ZP.Util.delete_localStorage('user-loggedin')
				ZP.Util.delete_localStorage('email')
				url = '/login'
				ZP.router.goto_url(url)
				return

		update_channels: ()=>
			_.keys(ZP.ChannelListeners).forEach (key)=>
				ZP.ChannelListeners[key].bind('new_post', @on_post_creation)

		select_channel: (channel)=>
			@selected_channel = channel.id

		on_channel_input_change: ()=>
			if $('.post-create-input-search').val() == ""
				@selected_channel = null

		on_new_post_creation_sucess: (data)=>
			$('.create-post-elem .create-post-input').val('')
			$('.create-post-elem .create-post-input').blur()
			$('.post-create-input-search').val('')
			$('.post-create-input-search').blur()
			console.log('on_new_post_creation_sucess', data)

		on_new_post_creation_failure: (err)=>
			console.log('on_new_post_creation_failure', err)

		create_post: (e)=>
			if $('.create-post-elem .create-post-input').val() == ""
				$('.create-post-elem .create-post-input').focus()

			if !@selected_channel
				$('.post-create-input-search').focus()

			if @selected_channel and  $('.create-post-elem .create-post-input').val() != ""
				obj =
					content: $('.create-post-elem .create-post-input').val()
					poster_id: ZP.User.get('id')
					channel_id: @selected_channel
					private: $('.create-post-elem .change-privacy-flag').data('private')
				$('.create-post-elem .create-post-input').val('')
				$('.post-create-input-search').val('')
				ZP.Dispatcher.trigger('new_post', obj, @on_new_post_creation_success, @on_new_post_creation_failure)


		change_privacy_flag: (e)=>
			if e and $(e.currentTarget)
				curr_stat = $(e.currentTarget).data('private')
				$(e.currentTarget).toggleClass('fade-color')
				if curr_stat == false
					$(e.currentTarget).data('private', "true")
				else if curr_stat == true
					$(e.currentTarget).data('private', "false")

		on_post_creation: (data)=>
			if data
				ZP.Feeds.add(data, {merge: true})
				ZP.Util.prepare_feed_response(ZP.Feeds.get(data.id))
				feed = ZP.Feeds.get(data.id)
				div = '<div class="feed-container" id="'+feed.get('id')+'" data-feedid="'+feed.get('id')+'">'
				div += @templates['feed'](feed: feed.toJSON(), user: ZP.User.toJSON())
				div += '</div>'
				$('.feed-list.inner-elems').prepend(div)
				ZP.event.trigger 'update:click_listener'

		destroy: ()=>
			@search_view.destroy()  if @search_view
			@textaera_type_view.destroy() if @search_view
			$('.create-post-elem .send-post-to-create-container').off('click', @create_post)
			$('.create-post-elem .create-post-input').off('focus')
			$('.create-post-elem .change-privacy-flag').off('click', @change_privacy_flag)
			@$el.removeData().unbind()
			@stopListening()

	return CreatePostView