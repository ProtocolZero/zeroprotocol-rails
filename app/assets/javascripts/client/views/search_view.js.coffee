define ['client/global_bus' , 'typeahead'], (ZP) ->

	class SearchView extends Backbone.View

		initialize: =>
			debugger
			@locality_input = @options.el
			@locality_input.typeahead('destroy').typeahead
				autoselect: true
				minLength: 0
				sections:
					name: 'results'
					source: @autocomplete
					highlight: true
					templates:
						suggestion: (datum) =>
							'<div class="' + datum.id + '">' + datum.value + '</div>'

			@locality_input.on('typeahead:selected', @goto_results)

		focus_input: () ->
			@locality_input.focus()

		autocomplete: (query, process) =>
			self = this
			suggestion = []
			ZP.Channels.toJSON().forEach (option) =>
				if option.name.toLowerCase().indexOf(query.trim().toLowerCase())!=-1 or query.length==0
					suggestion.push {value: option.name, id: option.id}

			if query.length == 0
				suggestion = suggestion.sort (a,b)=>
					if a.value > b.value
						return 1
					else
						return -1

			if query.length == 1
				suggestions = _.sortBy suggestion, (obj)->
					return obj.value.toLowerCase().indexOf(query.toLowerCase())

			process suggestion

		goto_results: (e, datum, header) =>
			e.stopPropagation()
			e.preventDefault()
			ZP.event.trigger 'channel:selected:'+@options.namespace, datum

		destroy: =>
			@stopListening()
			@locality_input.typeahead('destroy')
			@$el.removeData().unbind()

	return  SearchView
