define ['client/global_bus',
		'client/templates/client/_feed',
		'client/templates/client/_comment'
		], (ZP)->

	class FeedView extends Backbone.View

		el: '.feed-list'

		templates:
			feed: window.JST['client/feed']
			comment: window.JST['client/comment']

		initialize: ()=>
			@listenTo ZP.event, 'updated:feed', @update_feed
			@listenTo ZP.event, 'update:channels', @update_channels
			$('.feed-list .heart-icon-container').on('click', @update_like)
			$('.feed-list .feed-container .view-more-comments').on('click', @view_all_comments)
			@listenTo ZP.event,  'update:click_listener', @update_listener
			@update_channels()

		update_channels: ()=>
			_.keys(ZP.ChannelListeners).forEach (key)=>
				ZP.ChannelListeners[key].bind('new_unlike', @on_unlike)
			_.keys(ZP.ChannelListeners).forEach (key)=>
				ZP.ChannelListeners[key].bind('new_like', @on_like)

		update_listener: ()=>
			$('.feed-list .heart-icon-container').off('click', @update_like)
			$('.feed-list .heart-icon-container').on('click', @update_like)
			$('.feed-list .feed-container .view-more-comments').off('click', @view_all_comments)
			$('.feed-list .feed-container .view-more-comments').on('click', @view_all_comments)

		on_unlike_success : (data)=>
			console.log('on unlike success', data)

		on_like_success : (data)=>
			console.log('on like success', data)

		on_unlike_failure : (err)=>
			console.log('on unlike failure', err)

		on_like_failure : (err)=>
			console.log('on like failure', err)

		update_like: (e)=>
			if e and $(e.currentTarget) and $(e.currentTarget).data('feedid')
				feed_id  = $(e.currentTarget).data('feedid')
				is_liked = ZP.Feeds.get(feed_id).get('is_liked')
				obj =
					post_id: feed_id
					user_id: ZP.User.get('id')
					likes_count: ZP.Feeds.get(feed_id).get('likes_count')
				if is_liked
					ZP.Feeds.get(feed_id).set('is_liked', false)
					$(e.currentTarget).addClass('not-like')
					ZP.Dispatcher.trigger('new_unlike', obj, @on_unlike_success, @on_unlike_failure)
				else
					ZP.Feeds.get(feed_id).set('is_liked', true)
					$(e.currentTarget).removeClass('not-like')
					ZP.Dispatcher.trigger('new_like', obj, @on_like_success, @on_like_failure)

		on_unlike: (data)=>
			if data and data.post_id
				if data.likes_count < 0
					data.likes_count = 0
				if data.user_id == ZP.User.get('id')
					$('.feed-list .heart-icon-container[data-feedid='+data.post_id+']').addClass('not-like')

				$('.feed-list .like-count[data-feedid='+data.post_id+']').text(data.likes_count)

		view_all_comments: (e)=>
			if e and $(e.currentTarget)
				if $(e.currentTarget).hasClass('all')
					$(e.currentTarget).removeClass('all')
					id = $(e.currentTarget).data('feedid')
					$('.comment-container-inner[data-feedid='+id+'][data-hide=hide]').show()
				else if not  $(e.currentTarget).hasClass('all')
					$(e.currentTarget).addClass('all')
					id = $(e.currentTarget).data('feedid')
					$('.comment-container-inner[data-feedid='+id+'][data-hide=hide]').hide()

		on_like: (data)=>
			if data and data.post_id
				if data.likes_count < 0
					data.likes_count = 0
				if data.user_id == ZP.User.get('id')
					$('.feed-list .heart-icon-container[data-feedid='+data.post_id+']').removeClass('not-like')
				$('.feed-list .like-count[data-feedid='+data.post_id+']').text(data.likes_count)

				if data.user_id != ZP.User.get('id')
					ZP.Feeds.get(data.post_id).set('likes_count', data.likes_count)
					ZP.Util.prepare_feed_response(ZP.Feeds.get(data.post_id))
					feed = ZP.Feeds.get(data.post_id).toJSON()
					user = ZP.User.toJSON()
					$('.feed-list .feed-container[data-feedid='+data.post_id+']').remove()
					div = '<div class="feed-container" id="'+data.post_id+'" data-feedid="'+data.post_id+'">'
					div += @templates['feed'](feed: feed, user: user)
					div += '</div>'
					$('.feed-list').prepend(div)
					ZP.event.trigger 'update:click_listener'

		update_feed: (id)=>
			feed = ZP.Feeds.get(id)
			ZP.Util.prepare_feed_response(feed)
			feed = ZP.Feeds.get(id).toJSON()
			user = ZP.User.toJSON()
			$('.feed-list').find('.feed-container[data-feedid='+id+']').html(@templates['feed'](feed: feed, user: user))
			ZP.event.trigger 'update:click_listener'

		destroy: ()=>
			$('.feed-list .heart-icon-container').off('click', @update_like)
			@stopListening()

	return FeedView