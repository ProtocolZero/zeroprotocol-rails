define ['client/global_bus',
		'client/templates/client/_landing_screen'
		], (ZP)->

	class HomaPage extends Backbone.View

		el: '.home-container'

		templates:
			main: window.JST['client/landing_screen']

		initialize: ()=>
			if not ZP.is_user_loggedIn
				url = '/login'
				ZP.router.goto_url(url)
				return
			if !$('#container').find('.home-container').length
				$('#container').append(@templates['main']())

			$('body').find('#container').text('I came from Front End')

		destroy: ()=>
			@$el.removeData().unbind()
			@stopListening()
			@$el.remove()

	return HomaPage