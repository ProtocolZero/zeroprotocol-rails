define ['client/global_bus',
		'client/templates/client/_comment'
		], (ZP)->

	class CommentView extends Backbone.View

		templates:
			comment: window.JST['client/comment']

		initialize: ()=>
			@listenTo ZP.event, 'update:click_listener', @update_listener
			@listenTo ZP.event, 'update:channels', @update_channels
			$('.user-comment .user-comment-input').on('keydown', @on_comment_keydown)
			@update_channels()

		update_channels: ()=>
			_.keys(ZP.ChannelListeners).forEach (key)=>
				ZP.ChannelListeners[key].bind('new_comment', @on_new_comment)

		on_comment_post_success: (data)=>
			@target.val('')
			@target.blur()
			console.log('on_data_success', data)

		on_comment_post_failure: (err)=>
			@target.val('')
			@target.focus()
			console.log('could not post comment')

		update_listener: ()=>
			$('.user-comment .user-comment-input').off('keydown', @on_comment_keydown)
			$('.user-comment .user-comment-input').on('keydown', @on_comment_keydown)

		on_comment_keydown: (e)=>
			if e and e.which == 13 and $(e.currentTarget) and $(e.currentTarget).val() != ""
				e.stopPropagation()
				e.preventDefault()
				#:commenter_id => "", :post_id => "", :content => "", :private => true/false
				@target = $(e.currentTarget)
				post_id = $(e.currentTarget).data('feedid')
				content = $(e.currentTarget).val()
				privacy = $(e.currentTarget).parent().find('.change-privacy-flag').data('private')
				obj =
					commenter_id: ZP.User.get('id')
					post_id: post_id
					content: content
					private: privacy
				ZP.Dispatcher.trigger('new_comment', obj, @on_comment_post_success, @on_comment_post_failure)

		on_new_comment: (data)=>
			if data and data.post_id
				comments = ZP.Feeds.get(data.post_id).get('comments')
				new_comment = [data]
				comments = new_comment.concat(comments)
				ZP.Feeds.get(data.post_id).set('comments', comments)
				ZP.event.trigger('updated:feed', data.post_id)

			console.log('on new comment', data)


		destroy: ()=>
			$('.user-comment .user-comment-input').off('keydown', @on_comment_keydown)
			@stopListening()

	return CommentView