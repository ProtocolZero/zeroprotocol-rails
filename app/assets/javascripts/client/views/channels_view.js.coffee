define ['client/global_bus',
		'client/views/comment',
		'client/views/feed',
		'client/views/create_channel_view',
		'client/views/create_post_view',
		'client/templates/client/_web_ui',
		'client/templates/client/_feed',
		'client/templates/client/_comment',
		'client/templates/client/_channel'

		], (ZP, CommetView, FeedView, CreateChannelView, CreatePostView)->

	class ChannelsView extends Backbone.View

		el: '.main-container'

		templates:
			main: window.JST['client/web_ui']
			feed: window.JST['client/feed']
			comment: window.JST['client/comment']
			channel: window.JST['client/channel']

		initialize: ()=>
			if not ZP.is_user_loggedIn
				url = '/login'
				ZP.router.goto_url(url)
				return

			if $('#container').find('.main-container').length
				$('#container').find('.main-container').remove()
			ZP.Util.prepare_response()
			feeds = ZP.Feeds.toJSON()
			organisations = ZP.Organisation.toJSON()
			subscribed_channels = ZP.Channels.toJSON()
			unsubscribed_channels = ZP.Unsubscribed.toJSON()
			$('#container').append(@templates['main'](feeds: feeds, user: ZP.User.toJSON(), organisations: organisations, subscribed_channels: subscribed_channels, unsubscribed_channels: unsubscribed_channels))
			@create_channels()
			@comment_view = new CommetView()
			@feed_view = new FeedView()
			@create_channel_view = new CreateChannelView()
			@create_post_view = new CreatePostView()
			@listenTo ZP.event, 'update:click_listener', @update_listener
			$('.user-comment .change-privacy-flag').on('click', @toggle_privacy_data)
			ZP.Dispatcher.bind('feed_refresh', @refresh_feed)

		update_listener: ()=>
			$('.user-comment .change-privacy-flag').off('click', @toggle_privacy_data)
			$('.user-comment .change-privacy-flag').on('click', @toggle_privacy_data)

		create_channels: ()=>
			ZP.Channels.each (channel)=>
				ZP.ChannelListeners[channel.id] = ZP.Dispatcher.subscribe(channel.id)

		refresh_feed: (data)=>
			ZP.Feeds.reset()
			ZP.Feeds.add(data.feeds, {merge: true})
			ZP.Util.prepare_response()
			div = ''
			ZP.Feeds.each (feed)=>
				div += '<div class="feed-container" id="'+feed.get('id')+'" data-feedid="'+feed.get('id')+'">'
				div += @templates['feed'](feed: feed.toJSON(), user: ZP.User.toJSON())
				div += '</div>'
			if ZP.Feeds.length
				$('.feed-list.inner-elems').html(div)
			ZP.event.trigger 'update:click_listener'


		toggle_privacy_data: (e)=>
			if e and $(e.currentTarget)
				curr_stat = $(e.currentTarget).data('private')
				$(e.currentTarget).toggleClass('fade-color')
				if curr_stat == false
					$(e.currentTarget).data('private', "true")
				else if curr_stat == true
					$(e.currentTarget).data('private', "false")

		destroy: ()=>
			@$el.removeData().unbind()
			@comment_view.destroy()  if @comment_view
			ZP.ChannelListeners = {}
			@feed_view.destroy() if @feed_view
			@create_channel_view.destroy() if @create_channel_view
			@create_post_view.destroy() if @create_post_view
			$('.main-container').remove()
			@stopListening()

	return ChannelsView