define ['client/global_bus', 'client/helpers/timeago'], (ZP)->

	ZP.Util = {}

	ZP.Util.prepare_response = ()->
		ZP.Feeds.each (feed) ->
			ZP.Util.prepare_feed_response(feed)

	ZP.Util.prepare_feed_response = (feed)->
		feed.set('time', ZP.Util.format_time(feed.get('posted_at')))
		feed.set('response_prepared', true)
		feed.set('comments_two', false)
		feed.set('has_comments', false)
		if not feed.get('poster_name')
			feed.set('poster_name', 'Anonymous')
		feed.set('comments_count',feed.get('comments').length )
		if feed.get('comments') and feed.get('comments').length
			feed.set('has_comments', true)
			if feed.get('comments').length > 2
				feed.set('comments_two', true)
			feed.get('comments').forEach (comment)->
				if !comment.commenter_name?
					comment.commenter_name = 'Anonymous'
				if comment.commented_at
					comment.time = ZP.Util.format_time(comment.commented_at)

	ZP.Util.format_time = (time)->
		return $.timeago(time)

	ZP.Util.loadlocalStorage = () ->
		if ZP.Util.is_localstorage_accessible()
			ZP.localStorage = {}
			keys = _.keys window.localStorage
			_.each keys, (key)->
				ZP.localStorage[key] = window.localStorage[key]

	ZP.Util.read_localStorage = (type) ->
		if ZP.Util.is_localstorage_accessible()
			if not ZP.localStorage
				ZP.Util.loadlocalStorage()
			return ZP.localStorage[type]

	ZP.Util.write_localStorage = (type,value) ->
		if ZP.Util.is_localstorage_accessible()
			if not ZP.localStorage
				ZP.Util.loadlocalStorage()
			window.localStorage.setItem(type,value)
			ZP.localStorage[type] = value

	ZP.Util.delete_localStorage = (type) ->
		if ZP.Util.is_localstorage_accessible() and type
			delete window.localStorage[type]
			delete ZP.localStorage[type]

	ZP.Util.is_localstorage_accessible = () ->
		if ZP.is_localstorage_accessible != undefined
			return ZP.is_localstorage_accessible

		if not localStorage
			ZP.is_localstorage_accessible = false
		else
			try
				localStorage['ZP-key'] = 'Zero Protocol'
			catch e
				ZP.is_localstorage_accessible = false

			if localStorage['ZP-key'] == 'Zero Protocol'
				ZP.is_localstorage_accessible = true
			else
				ZP.is_localstorage_accessible = false

		return ZP.is_localstorage_accessible

	ZP.Util.user_login_success = (data)->
		if data
			if data.channels and data.channels.length
				ZP.Channels.add(data.channels, {merge: true})
				ZP.ChannelListeners = {}
			if data.feeds and data.feeds.length
				ZP.Feeds.add(data.feeds, {merge: true})

			if data.unsubscribed_channels and data.unsubscribed_channels.length
				ZP.Unsubscribed.add(data.unsubscribed_channels, {merge: true})

			if data.organisations and data.organisations.length
				ZP.Organisation.add(data.organisations, {merge: true})

			if data.user
				ZP.User.set('id',data.user.id , {silent: true})
				ZP.User.set('name',data.user.name , {silent: true})
				ZP.User.set('email',data.user.email, {silent: true})
				ZP.User.set('number', data.user.contact_no, {silent: true})
				if ZP.Util.is_localstorage_accessible()
					ZP.Util.write_localStorage('user-loggedin',true )
					ZP.Util.write_localStorage('email',data.user.email )

		ZP.is_user_loggedIn = true
		url = '/user-feed'
		ZP.router.goto_url(url)

	ZP.Util.user_login_failure = (err)->
		url = '/login'
		ZP.router.goto_url(url)

	ZP.Util.make_call= (email)->
		user_details = {email: email}
		ZP.Dispatcher.trigger('user_loggedin', user_details, ZP.Util.user_login_success, ZP.Util.user_login_failure)

	ZP