require ['underscore',
		'jquery',
		'backbone',
		'client/router/router',
		'client/global_bus',
		'websocket_rails/main'
		],(_, $, B, Router, ZP) ->

		String.prototype.capitalize = () ->
			return this.charAt(0).toUpperCase() + this.slice(1)

		router = new Router options
		ZP.socket_url = url = options.host+':'+options.port+'/websocket'
		# ZP.socket_url = url = 'kailash.housing.com:3632/websocket'
		use_websockets = true
		ZP.Dispatcher  = new WebSocketRails(url, use_websockets)
		ZP.Dispatcher.onclose = ()=>
			ZP.Dispatcher.reconnect()
			ZP.Dispatcher.reconnect_channels()
			console.log(ZP.Dispatcher.queue)

		ZP.router = router
		Backbone.history.start({pushState: true})

