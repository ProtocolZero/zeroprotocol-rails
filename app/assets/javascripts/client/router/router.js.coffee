define ['client/global_bus',
		'client/models/user',
		'client/models/posts',
		'client/models/channels',
		'client/views/Util',
		'hamlcoffee'
		], (ZP, User, Post, Channel) ->

	class Router extends Backbone.Router

		routes:
			""			: 	"index"
			"user-feed" :   "on_channel"
			"login"		: 	"on_login"

		initialize: (options)->
			ZP.is_user_loggedIn = false
			ZP.User = new User()
			ZP.Feeds = new Post.Collection()
			ZP.Channels = new Channel.Collection()
			ZP.Unsubscribed = new Channel.Collection()
			ZP.Organisation = new Channel.Collection()

		index: (qp)=>
			@start_views('home')

		start_views: (view)=>
			if view == 'home'
				path = 'client/views/home_page'
			else if view == 'channels'
				path = 'client/views/channels_view'
			else if view == 'login'
				path = 'client/views/login_view'

			@change_site_state()

			if path
				arr = [path]
				require arr, (IndexView)=>
					if !@index_view?
						@index_view = new IndexView()
					else if @index_view? and not (@index_view instanceof IndexView)
						@index_view.destroy()
						@index_view = new IndexView()
					else
						ZP.event.trigger 'loaded:view'

		on_channel: (qp)=>
			if @site_state
				@prev_site_state = @site_state
			@site_state = 'home'
			@start_views('channels')

		on_login: (qp)=>
			if @site_state
				@prev_site_state = @site_state
			@site_state = "login"
			@change_site_state()
			ZP.is_user_loggedIn = false
			@start_views('login')

		check_logged_in: ()=>
			if ZP.Util.is_localstorage_accessible() and ZP.Util.read_localStorage('user-loggedin')
				email =  ZP.Util.read_localStorage('email')
				ZP.Util.make_call(email)
				return true
			return

		change_site_state: ()=>
			if @prev_site_state
				$('body').removeClass(@prev_site_state)
			if @site_state
				$('body').addClass(@site_state)

		goto_url: (url, trigger=true, replace=false, complete_url = false) =>
			if url and complete_url
				@navigate(url,{trigger:trigger, replace:replace})
			else if url
				# url = @toFragment(url)
				@navigate(url,{trigger:trigger, replace:replace})
			else
				herr 'cannot go to null url'

	return Router

