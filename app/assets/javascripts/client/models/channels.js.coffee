define ['client/global_bus'], (ZP) ->
	class ChannelsModel extends Backbone.Model
		defaults:
			id: null

		initialize: () ->
			self = this

		url: ()->

	class ChannelsCollection extends Backbone.Collection
		model: ChannelsModel

		initialize: ()->


	return {
		Model: ChannelsModel
		Collection: ChannelsCollection
	}