define ['client/global_bus'], (ZP) ->
	class UserModel extends Backbone.Model
		defaults:
			email: null

		initialize: () ->
			self = this

		url: ()->

	return UserModel
