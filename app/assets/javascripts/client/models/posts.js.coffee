define ['client/global_bus'], (ZP) ->
	class PostModel extends Backbone.Model
		defaults:
			id: null

		initialize: () ->
			self = this

		url: ()->

	class PostCollection extends Backbone.Collection
		model: PostModel

		initialize: ()->


	return {
		Model: PostModel
		Collection: PostCollection
	}