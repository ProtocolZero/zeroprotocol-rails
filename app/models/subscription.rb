class Subscription < ActiveRecord::Base
  attr_accessible :user_id, :channel_id
  validates :user_id, :channel_id, :presence => true
  validates_uniqueness_of :user_id, :scope => :channel_id, :message => "User can subscribe to a channel only once!"
  belongs_to :user
  belongs_to :channel
end
