class Organisation < ActiveRecord::Base
	attr_accessible :name, :email_identifier
  validates :name, :email_identifier, :presence => true, :uniqueness => true
end
