class Channel < ActiveRecord::Base
  attr_accessible :name, :organisation_id
  validates :name, :presence => true, :uniqueness => true
  validates :organisation_id, :presence => true
  belongs_to :organisation
  has_many :subscriptions
  has_many :users, :through => :subscriptions
  has_many :posts
end
