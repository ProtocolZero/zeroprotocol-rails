class User < ActiveRecord::Base
	attr_protected :password
  validates :name, :presence => true
  validates :email, :presence => true, :uniqueness => true #ToDo:: put email validator
  validates :contact_no, :presence => true, :uniqueness => true #ToDo:: put email validator
  validates :organisation_id, :presence => true

  belongs_to :organisation
  has_many :subscriptions
  has_many :channels, :through => :subscriptions
  has_many :posts, :foreign_key => "poster_id" #Posted By User

  def subscribed_channels
  	self.channels.joins{users.outer}.group(:id).select("channels.id, channels.name, COUNT(users.id)")
  end

  def unsubscribed_channels
  	subscribed_channels = self.channels.select("channels.id").collect(&:id)
  	Channel.where.not(:id => subscribed_channels)
			  	 .joins{users.outer}.group(:id)
			  	 .select("channels.id, channels.name, COUNT(users.id)")
  end

=begin
	posts: [{
		id:
		content:
		poster_name:
		channel_name:
		posted_at:
		comments:[{
			commenter_name:
			content:
			commented_at:
			post_id:
		}]
	}]
=end
  def latest_feed
  	feeds = User.joins(:channels)
				  			.where("subscriptions.user_id = ? ", self.id)
				  			.joins("INNER JOIN posts ON posts.channel_id = channels.id")
				  			.joins("INNER JOIN users poster ON poster.id = posts.poster_id")
				  			.order("posts.updated_at DESC")
				  			
		comments = feeds.joins("INNER JOIN comments ON comments.post_id = posts.id")
										.joins("INNER JOIN users commenter ON commenter.id = comments.commenter_id")
										.select("(CASE WHEN comments.private = 't' THEN NULL ELSE initcap(commenter.name) END) as commenter_name")
										.select("comments.id, comments.content, comments.created_at as commented_at, comments.post_id")
										.order("comments.created_at DESC")
										.as_json

		likes = Like.group(:post_id).select("post_id, COUNT(id) as likes_count").as_json
		post_liked = Like.where(user_id: self.id).select("post_id").collect(&:post_id)
		feeds	=	feeds.select("posts.id, posts.content, channels.id as channel_id, channels.name as channel_name, posts.created_at as posted_at")
				  			 .select("(CASE WHEN posts.private = 't' THEN NULL ELSE initcap(poster.name) END) as poster_name")
				  			 .map{|feed|
				  			 	feed = feed.as_json
				  			 	feed["comments"] = comments.select{|c| c["post_id"] == feed["id"]}
				  			 	feed["likes_count"] = likes.select{|c| c["post_id"] == feed["id"]}[0]["likes_count"] rescue 0
				  			 	feed["is_liked"] = post_liked.include? feed["id"]
				  			 	feed
				  			 }
  end
end
