class Like < ActiveRecord::Base
	attr_accessible :post_id, :user_id
  validates :user_id, :post_id, :presence => true
  validates_uniqueness_of :post_id, :scope => :user_id, :message => "User can like a post only once!"
  belongs_to :post#, :touch => true
  belongs_to :user

  validate :channel_validation

  after_save :touch_post

  def touch_post
  	self.post.touch(:updated_at)
  end

  def channel_validation
  	if self.user
  		if self.post
		  	unless self.user.channels.include? self.post.channel
		  		errors.add(:base, "user is not subscribed to this channel!")
		  	end
		  else
  			errors.add(:base, "post is not valid!")
		  end
  	else
  		errors.add(:base, "user is not valid!")
  	end
  end
end
