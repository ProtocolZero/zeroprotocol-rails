class Post < ActiveRecord::Base
	attr_accessible :content, :poster_id, :channel_id, :private
  validates :content, :poster_id, :channel_id, :presence => true
  belongs_to :channel
  belongs_to :poster, :class_name => "User"
  has_many :comments
  has_many :likes
  validate :channel_validation

  def channel_validation
  	if self.poster
	  	unless self.poster.channels.include? self.channel
	  		errors.add(:base, "user is not subscribed to this channel!")
	  	end
	  else
	  	errors.add(:base, "poster is not valid!")
	  end
  end

  def formatted_info_for_new_post
    Post.joins(:channel)
        .where("posts.id = ?", self.id)
        .joins("INNER JOIN users poster ON poster.id = posts.poster_id")
        .select("posts.id, posts.content, channels.id as channel_id, channels.name as channel_name, posts.created_at as posted_at")
        .select("(CASE WHEN posts.private = 't' THEN NULL ELSE initcap(poster.name) END) as poster_name")
        .map{|feed|
          feed = feed.as_json
          feed["comments"] = []
          feed["likes_count"] = 0
          feed["is_liked"] = false
          feed
         }[0]
  end
end
