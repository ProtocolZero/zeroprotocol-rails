class Comment < ActiveRecord::Base
	attr_accessible :content, :post_id, :commenter_id, :private
  validates :content, :post_id, :commenter_id, :presence => true
  belongs_to :post#, :touch => true
  belongs_to :commenter, :class_name => "User"

  validate :channel_validation

  after_save :touch_post

  def touch_post
  	self.post.touch(:updated_at)
  end

  def channel_validation
  	if self.commenter
  		if self.post
		  	unless self.commenter.channels.include? self.post.channel
		  		errors.add(:base, "user is not subscribed to this channel!")
		  	end
		  else
  			errors.add(:base, "post is not valid!")
		  end
  	else
  		errors.add(:base, "commenter is not valid!")
  	end
  end
end
