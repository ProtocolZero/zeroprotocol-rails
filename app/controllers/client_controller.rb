class ClientController < ApplicationController

	before_filter do |controller|
		actions = []
		if actions.length and not actions.include?(controller.action_name)
			initializers
		end
	end

	def index
		render '_landing_screen'
	end

	def channels
		@site_state = "home"
		render '_web_ui', :feeds => [], :user => false
	end
	def login
		@noHeader = true
		@site_state = "login"
		render '_login'
	end

	def doLogin

	end

	def initializers
		@host = request.host
		@port = request.port
	end

end