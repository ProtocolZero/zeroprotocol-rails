class EventController < WebsocketRails::BaseController
  include ActionView::Helpers::SanitizeHelper

  def initialize_session
    Rails.logger.debug "kailash::  initialize_session"
  end

  def client_connected
    Rails.logger.debug "kailash::  client_connected"
  end

  def client_disconnected
    Rails.logger.debug "kailash::  client_disconnected"
  end

=begin
message:{:email => "email id of the loggedin_user"}
=end
  def user_loggedin
    Rails.logger.debug "kailash::  user_loggedin"
    user = User.where(email: sanitize(message[:email])).first
    if user
      user_details = user.as_json(:only => [:id, :name, :email, :contact_no])
      user_details["name"] = user_details["name"].titlecase
      response = {
        :status => 200,
        :message => "ok",
        :user => user_details,
        :channels => user.subscribed_channels,
        :unsubscribed_channels => user.unsubscribed_channels,
        :feeds => user.latest_feed ,
        :organisations => Organisation.select("id,name").as_json
      }
      trigger_success response
    else
      response = {
        :status => 404,
        :message => "User Not Found"
      }
      trigger_failure response
    end
  end

=begin
message:{:poster_id => "", :channel_id => "', :content => "", :private => true/false}
=end
  def new_post
    Rails.logger.debug "kailash::  new_post"
    post = Post.new(message.as_json(:only => [:poster_id, :channel_id, :content, :private]))
    if post.save
      WebsocketRails[message[:channel_id]].trigger(:new_post, post.formatted_info_for_new_post)
      response = {
        :status => 200,
        :message => "ok"
      }
      trigger_success response
    else
      response = {
        :status => 422,
        :message => post.errors
      }
      trigger_failure response
    end
  end
=begin
message:{:commenter_id => "", :post_id => "", :content => "", :private => true/false}
=end
  def new_comment
    Rails.logger.debug "kailash::  new_comment"
    comment = Comment.new(message.as_json(:only => [:commenter_id, :post_id, :content, :private]))
    if comment.save
      WebsocketRails[comment.post.channel_id].trigger(:new_comment, {
        :id => comment.id,
        :content => comment.content,
        :post_id => comment.post_id,
        :commenter_name => comment.private ? nil : comment.commenter.name,
        :commented_at => comment.created_at
        })
      response = {
        :status => 200,
        :message => "ok"
      }
      trigger_success response
    else
      response = {
        :status => 422,
        :message => comment.errors
      }
      trigger_failure response
    end
  end

=begin
message:{:post_id => "", :user_id => "", :likes_count=> ""}
=end
  def new_like
    Rails.logger.debug "kailash::  new_like"
    like = Like.new(message.as_json(:only => [:post_id, :user_id]))
    if like.save
      WebsocketRails[like.post.channel_id].trigger(:new_like, {
        :post_id => like.post_id,
        :user_id => like.user_id,
        :likes_count => message[:likes_count] + 1
      })
      response = {
        :status => 200,
        :message => "ok"
      }
      trigger_success response
    else
      response = {
        :status => 422,
        :message => like.errors
      }
      trigger_failure response
    end
  end

=begin
message:{:post_id => "", :user_id => "", :likes_count=> ""}
=end
  def new_unlike
    Rails.logger.debug "kailash::  new_unlike"
    like = Like.where(message.as_json(:only => [:post_id, :user_id])).first
    if like && like.destroy
      post = Post.find(message[:post_id])
      WebsocketRails[post.channel_id].trigger(:new_unlike, {
        :post_id => message[:post_id],
        :user_id => message[:user_id],
        :likes_count => message[:likes_count] - 1
      })
      response = {
        :status => 200,
        :message => "ok"
      }
      trigger_success response
    else
      response = {
        :status => 422,
        :message => like.nil? ? "Like not found!" : like.errors
      }
      trigger_failure response
    end
  end

=begin
message:{:channel_id => "", :user_id => ""}
=end
  def channel_subscribe
    Rails.logger.debug "kailash::  channel_subscribe"
    subs = Subscription.new(message.as_json(:only => [:channel_id, :user_id]))
    if subs.save
      channel = subs.channel
      send_message :feed_refresh, {
        :feeds => subs.user.latest_feed
      }
      broadcast_message :channel_subscribe, {
        :id => channel.id,
        :name => channel.name,
        :count => message[:count] + 1,
        :user_id => message[:user_id],
      }
      response = {
        :status => 200,
        :message => "ok"
      }
      trigger_success response
    else
      response = {
        :status => 422,
        :message => subs.errors
      }
      trigger_failure response
    end
  end

=begin
message:{:channel_id => "", :user_id => ""}
=end
  def channel_unsubscribe
    Rails.logger.debug "kailash::  channel_unsubscribe"
    subs = Subscription.where(message.as_json(:only => [:channel_id, :user_id])).first
    if subs && subs.destroy
      channel = Channel.find(message[:channel_id])
      broadcast_message :channel_unsubscribe, {
        :id => channel.id,
        :name => channel.name,
        :count => message[:count] - 1,
        :user_id => message[:user_id]
      }
      response = {
        :status => 200,
        :message => "ok"
      }
      trigger_success response
    else
      response = {
        :status => 422,
        :message => subs.nil? ? "Subs not found!" : subs.errors
      }
      trigger_failure response
    end
  end
=begin
message:{:name => "", :organisation_id => "", :user_id => ""}
=end
  def create_channel
    Rails.logger.debug "kailash::  create_channel"
    channel = Channel.new(message.as_json(:only => [:name, :organisation_id]))
    if channel.save
      subs = Subscription.create(:channel_id => channel.id, :user_id => message[:user_id])
      broadcast_message :create_channel, {
        :id => channel.id,
        :name => channel.name,
        :count => 1,
        :user_id => message[:user_id]
      }
      response = {
        :status => 200,
        :message => "ok"
      }
      trigger_success response
    else
      response = {
        :status => 422,
        :message => channel.errors
      }
      trigger_failure response
    end
  end
end