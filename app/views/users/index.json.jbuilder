json.array!(@users) do |user|
  json.extract! user, :id, :name, :email, :contact_no, :password, :organisation_id, :last_login
  json.url user_url(user, format: :json)
end
