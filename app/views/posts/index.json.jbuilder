json.array!(@posts) do |post|
  json.extract! post, :id, :poster_id, :channel_id, :content, :private
  json.url post_url(post, format: :json)
end
