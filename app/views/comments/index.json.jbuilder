json.array!(@comments) do |comment|
  json.extract! comment, :id, :commenter_id, :post_id, :content, :private
  json.url comment_url(comment, format: :json)
end
